#!/usr/bin/env python3

import argparse
import os
import json
import email
import unicodedata
from email.parser import BytesHeaderParser
from email.parser import HeaderParser
from email.header import decode_header

def get_subject(file):
    with open(file, 'rb') as fp:
        hdr = BytesHeaderParser().parse(fp)
        subj = decode_header(hdr['Subject'])
        return (str(subj[0][0]))

def to_me(file):
    with open(file, 'rb') as fp:
        hdr = BytesHeaderParser().parse(fp)
        hdr_to = hdr['To']
        if hdr_to is None:
            return False
        to_list = decode_header(hdr['To'])[0]
        # This seems the best unicode case insensitive match
        me = unicodedata.normalize("NFKD", "widawsky".casefold())
        for recipient in to_list:
            to = unicodedata.normalize("NFKD", str(recipient).casefold())
            if me in to:
                return True
        return False

def cc_me(file):
    with open(file, 'rb') as fp:
        hdr = BytesHeaderParser().parse(fp)
        hdr_to = hdr['Cc']
        if hdr_to is None:
            return False
        to_list = decode_header(hdr['Cc'])[0]
        # This seems the best unicode case insensitive match
        me = unicodedata.normalize("NFKD", "widawsky".casefold())
        for recipient in to_list:
            to = unicodedata.normalize("NFKD", str(recipient).casefold())
            if me in to:
                return True
        return False


def dir_path(string):
    if os.path.isdir(string):
        return string
    else:
        raise NotADirectoryError(string)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Mindless mail subjects")
    parser.add_argument("maildir", type=dir_path,
                        help="Path to the maildir. Likely will be something "
                             "like: $HOME/mail/INBOX/new")
    args = parser.parse_args()

    data = {}
    data["class"] = "mail"
    subjects = []
    count_to_me = 0
    count_cc_me = 0
    for dirpath, dirnames, files in os.walk(args.maildir):
        count = len(files)
        for file in files:
            if to_me(os.path.join(dirpath, file)):
                count_to_me = count_to_me + 1
            if cc_me(os.path.join(dirpath, file)):
                count_cc_me = count_cc_me + 1
            subjects.append(get_subject(os.path.join(dirpath, file)))
        data["text"] = str(count) + " (" + str(count_to_me + count_cc_me) + ")"

    if count:
        data["percentage"] = 100
        data["tooltip"] = '\n'.join(["- " + str(i) for i in subjects])
    else:
        data["percentage"] = 0
        data["text"] = 0

    print(json.dumps(data))
